const mysql = require('mysql');

const connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'olenabrautmode'
});
/*
const connection = mysql.createConnection({
	host: 'localhost',
	user: 'admin_alexgum',
	password: 'ichi785612',
	database: 'admin_olenabrautmode'
});
*/
connection.connect((err) => {
	if(err){
		throw err;
	}
	console.log('MySql connected...');
});


connection.query("SET SESSION wait_timeout = 604800"); // 7 days timeout



module.exports = connection;