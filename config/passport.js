const LocalStrategy = require('passport-local').Strategy;
const db = require('./DBconnection');


module.exports = function (passport) {
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function (id, done) {
    db.query("SELECT * FROM clients WHERE id = ? ", [id], function (err, rows) {
        done(err, rows[0]);
      });
  });

  passport.use(
    'local-signup', new LocalStrategy({
      usernameField: 'clientname',
      passwordField: 'email',
      passReqToCallback: true
    },
      function (req, clientname, email, done) {
        let sql = 'SELECT * FROM clients WHERE name = ?';
        let query = db.query(sql, clientname, (err, rows) => {
          if (err) throw err;
          if (rows.length) {
            return done(null, false, req.flash('loginMessage', 'Вы уже зарегистриреванны'));
          } else {
            let newUserMysql = {
              Name: clientname,
              email: email,
              phone: req.body.phoneForm
            };

            let insertQuery = 'INSERT INTO clients SET ?';

            db.query(insertQuery, [newUserMysql], (err, rows) => {
              newUserMysql.id = rows.insertId;

              return done(null, newUserMysql);
            });
          }
        });
      })
  );

  passport.use(
    'local-login', new LocalStrategy({
      usernameField: 'clientname',
      passwordField: 'email',
      passReqToCallback: true
    },
      function (req, clientname, email, done) {
        db.query("SELECT * FROM clients WHERE name = ? ", [clientname],
          function (err, rows) {
            if (err)
              return done(err);
            if (!rows.length) {
              return done(null, false, req.flash('loginMessage', 'No User Found'));
            }
            if (!rows[0].email)
              return done(null, false, req.flash('loginMessage', 'Wrong Password'));

            return done(null, rows[0]);
          });
      })
  );


};