-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 10 2018 г., 02:49
-- Версия сервера: 5.7.20
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `olenabrautmode`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categori`
--

CREATE TABLE `categori` (
  `id` int(11) NOT NULL,
  `TitleCat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categori`
--

INSERT INTO `categori` (`id`, `TitleCat`) VALUES
(1, 'braut'),
(2, 'abend'),
(3, 'kinder');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `termin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `dress`
--

CREATE TABLE `dress` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `silhuette_id` int(11) NOT NULL,
  `categoti_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dress`
--

INSERT INTO `dress` (`id`, `Name`, `Description`, `image`, `silhuette_id`, `categoti_id`) VALUES
(28, 'qweerty123', 'qweerty', 'photo-1516483156299-a0ecd4226c08.jpg', 2, 1),
(30, 'fsdfsf', 'sfsfs', 'bg_head.jpg', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `silhouette`
--

CREATE TABLE `silhouette` (
  `id` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `silhouette`
--

INSERT INTO `silhouette` (`id`, `Title`) VALUES
(1, 'Meerjungfrau'),
(2, 'A-Linie'),
(3, 'A-Linie / Prinzessin');

-- --------------------------------------------------------

--
-- Структура таблицы `termin`
--

CREATE TABLE `termin` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `termindetails_id` int(11) NOT NULL,
  `data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `termindetails`
--

CREATE TABLE `termindetails` (
  `id` int(11) NOT NULL,
  `termin_id` int(11) NOT NULL,
  `dress_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categori`
--
ALTER TABLE `categori`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `termin_id` (`termin_id`);

--
-- Индексы таблицы `dress`
--
ALTER TABLE `dress`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoti_id` (`categoti_id`),
  ADD KEY `silhuette_id` (`silhuette_id`);

--
-- Индексы таблицы `silhouette`
--
ALTER TABLE `silhouette`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `termin`
--
ALTER TABLE `termin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `termindetails_id` (`termindetails_id`);

--
-- Индексы таблицы `termindetails`
--
ALTER TABLE `termindetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dress_id` (`dress_id`),
  ADD KEY `termin_id` (`termin_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categori`
--
ALTER TABLE `categori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `dress`
--
ALTER TABLE `dress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `silhouette`
--
ALTER TABLE `silhouette`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `termin`
--
ALTER TABLE `termin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `termindetails`
--
ALTER TABLE `termindetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`termin_id`) REFERENCES `termin` (`id`);

--
-- Ограничения внешнего ключа таблицы `dress`
--
ALTER TABLE `dress`
  ADD CONSTRAINT `dress_ibfk_1` FOREIGN KEY (`categoti_id`) REFERENCES `categori` (`id`),
  ADD CONSTRAINT `dress_ibfk_2` FOREIGN KEY (`silhuette_id`) REFERENCES `silhouette` (`id`);

--
-- Ограничения внешнего ключа таблицы `termin`
--
ALTER TABLE `termin`
  ADD CONSTRAINT `termin_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `termin_ibfk_2` FOREIGN KEY (`termindetails_id`) REFERENCES `termindetails` (`id`);

--
-- Ограничения внешнего ключа таблицы `termindetails`
--
ALTER TABLE `termindetails`
  ADD CONSTRAINT `termindetails_ibfk_1` FOREIGN KEY (`dress_id`) REFERENCES `dress` (`id`),
  ADD CONSTRAINT `termindetails_ibfk_2` FOREIGN KEY (`termin_id`) REFERENCES `termin` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
