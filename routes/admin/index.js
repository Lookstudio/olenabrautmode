const express = require('express');
const router = express.Router();
const { userAuthenticated} = require('../../helpers/authentication');
const db = require('../../config/DBconnection');


router.all('/*', (req, res, next) => {

    req.app.locals.layout = 'admin';
    next();

});

router.get('/',  (req, res) =>{
    let viewCliets = new Promise((resolve, reject) =>{
        let sqlClients = 'SELECT * FROM clients';
        let query = db.query(sqlClients, (err, rows) => {
            if(err) throw err;
            resolve(rows)
        })
    })
    let viewTermins = new Promise((resolve, reject) =>{
        let sqlClients = 'SELECT clients.Name, clients.phone, dress.DressName, termin.data, termin.tarif FROM clients JOIN termin ON termin.client_id = clients.id JOIN termindetails ON termindetails.termin_id = termin.id JOIN dress ON dress.id = termindetails.dress_id';
        let query = db.query(sqlClients, (err, rows) => {
            if(err) throw err;
            resolve(rows)
        })
    })
    Promise.all([viewCliets, viewTermins]).then((value) =>{
        let clientsCounts = value[0].length;
        let TerminCounts = value[1].length;

        res.render('admin/index', {clientsCounts: clientsCounts, TerminCounts: TerminCounts});
    })

})

router.get('/clients', (req, res) =>{
    let sql = 'SELECT * FROM clients';
    let query = db.query(sql, (err, rows) => {
        if(err) throw err;
        console.log(rows);
	res.render('admin/client', {data: rows});
    })
})

router.get('/termin', (req, res) =>{
    let sql = 'SELECT clients.Name, clients.phone, dress.DressName, termin.data, termin.tarif FROM clients JOIN termin ON termin.client_id = clients.id JOIN termindetails ON termindetails.termin_id = termin.id JOIN dress ON dress.id = termindetails.dress_id';
    let query = db.query(sql, (err, rows) => {
        if(err) throw err;
        console.log(rows);

    res.render('admin/termin', {data: rows});
})
})

function notLoggedIn(req, res, next){
    if(!req.isAuthenticated()){
        return next();
    }
    

res.redirect('/');
}


module.exports = router;