const express = require('express');
const router = express.Router();
const { isEmpty, uploadDir } = require('../../helpers/upload-helper');
const fs = require('fs');
const path = require('path');
const urlSlug = require('url-slug');
const gm = require('gm').subClass({imageMagick: true});
const jimp = require('jimp');

const db = require('../../config/DBconnection');
const { userAuthenticated } = require('../../helpers/authentication');

router.all('/*',  (req, res, next) => {

    req.app.locals.layout = 'admin';
    next();

});




router.get('/', (req, res) => {
    let sql = 'SELECT dress.id, dress.DressName, dress.Description, images.img_title, silhouette.Title, categori.TitleCat FROM dress JOIN silhouette ON dress.silhuette_id = silhouette.id JOIN categori ON dress.categoti_id=categori.id JOIN images ON images.dress_id = dress.id';
    let query = db.query(sql, (err, rows) => {
        if(err) throw err;
      
        res.render('admin/dress/all', {data: rows})
    })
})

router.get('/create', (req, res) =>{
    res.render('admin/dress/create');
})

router.post('/create/add', (req, res) =>{
    
    let input = req.body;

    if(!isEmpty(req.files)){

        let fileTitle = req.files.fileTitle;
        let fileLeft = req.files.fileLeft;
        let fileRight = req.files.fileRight;
        let fileTop = req.files.fileTop;
        
        fileTitle.mv('./public/uploads/' + input.name + '-title.jpg', (err) => {
            if(err) throw err;
        }) 
        fileLeft.mv('./public/uploads/' + input.name + '-left.jpg', (err) => {
            if(err) throw err;
        }); 
        fileRight.mv('./public/uploads/' + input.name + '-right.jpg', (err) => {
            if(err) throw err;
        }); 
        fileTop.mv('./public/uploads/' + input.name + '-top.jpg', (err) => {
            if(err) throw err;
        }); 
    }


    let data = {
        DressName: input.name,
        Description: input.discription,
        silhuette_id: input.silhouette,
        categoti_id: input.categori, 
        slug: urlSlug(input.name)
        };

    let sql = 'INSERT INTO dress SET ?';
    let query = db.query(sql, [data], (err, result) => {
        if(err) throw err;
        req.flash('succes-message', `Платье ${data.Name} добавленно!`);
        res.redirect('/admin/dress');

        let img = {
            dress_id: result.insertId,
            img_title: input.name + '-title.jpg',
            img_right: input.name + '-left.jpg',
            img_left: input.name + '-right.jpg',
            img_top: input.name + '-top.jpg'
            }
    
            let sqlImage = 'INSERT INTO images SET ?';
            let queryImg = db.query(sqlImage, [img], (err, row) =>{
                if(err) throw err;
            })
    
    })

})




router.get('/edit/:id', (req, res) => {
    let id = req.params.id;
    let sql = `SELECT * FROM dress WHERE id = ${id}`;
    let query = db.query(sql, (err, rows) => {
        if(err) throw err;

        //res.send(rows);
        res.render('./admin/dress/edit_dress', {data: rows});

    });
})

router.post('/edit/:id', (req, res) =>{

    let input = req.body;
    let id = req.params.id;

    let data = {
        DressName: input.name,
        Description: input.discription,
        silhuette_id: input.silhouette,
        categoti_id: input.categori,  
    
    };

    if(!isEmpty(req.files)){

        let file = req.files.file;
        filename = file.name;
        data.image = filename
        
        file.mv('./public/uploads/' + input.name + '.jpg', (err) => {
            if(err) throw err;
        }); 
        fileLeft.mv('./public/uploads/' + input.name + '-left.jpg', (err) => {
            if(err) throw err;
        }); 
        fileRight.mv('./public/uploads/' + input.name + '-right.jpg', (err) => {
            if(err) throw err;
        }); 
        fileTop.mv('./public/uploads/' + input.name + '-top.jpg', (err) => {
            if(err) throw err;
        }); 
    
    }

    let sql = `UPDATE dress SET ? WHERE id = ${id}`;
    let query = db.query(sql, [data], (err, result) => {
        if(err) throw err;

        req.flash('succes-message', `Платье ${data.Name} обновленно!`);
        res.redirect('/admin/dress');
    })
})

router.get('/delete/:id', (req, res) =>{
    let id = req.params.id;
    let sql = `DELETE FROM dress WHERE id = ${id}`;
    let query = db.query(sql, (err, result) => {
        if(err) throw err;
        req.flash('danger-message', 'Платье удалено');

        res.redirect('/admin/dress');
    })
})

module.exports = router;
