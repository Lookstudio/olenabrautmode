const express = require('express');
const router = express.Router();
const db = require('../../config/DBconnection');



router.all('/*', (req, res, next) => {

    req.app.locals.layout = 'home';
    next();

});


router.get('/', (req, res) =>{
	
	let sql = 'SELECT dress.id, dress.slug, dress.DressName, dress.Description, images.img_title, images.img_right, images.img_left, images.img_top, silhouette.Title, categori.TitleCat FROM dress JOIN silhouette ON dress.silhuette_id = silhouette.id JOIN categori ON dress.categoti_id=categori.id JOIN images ON images.dress_id = dress.id';
    let query = db.query(sql, (err, rows) => {
		if(err) throw err;

		
		res.render('home/kategorien/braut', {data: rows});
	})
})

router.get('/:slug', (req, res) =>{
    let slug = req.params.slug;
    let sql = `SELECT dress.id, dress.DressName, dress.slug, dress.Description, images.img_title, images.img_right, images.img_left, images.img_top, silhouette.Title, categori.TitleCat FROM dress JOIN silhouette ON dress.silhuette_id = silhouette.id JOIN categori ON dress.categoti_id=categori.id JOIN images ON images.dress_id = dress.id WHERE dress.slug = '${slug}'`;
    let query = db.query(sql, (err, rows) => {
		if(err) throw err;
        console.log('rows', rows);
		

    res.render('home/kategorien/brautcard', {data: rows});
    })
})



module.exports = router;