const express = require('express');
const router = express.Router();
const db = require('../../config/DBconnection');
const Cart = require('../../models/Cart');

router.all('/*', (req, res, next) => {

    req.app.locals.layout = 'home';
    next();

});
let data;
db.query('SELECT dress.id, dress.DressName, dress.Description, images.img_title, silhouette.Title, categori.TitleCat FROM dress JOIN silhouette ON dress.silhuette_id = silhouette.id JOIN categori ON dress.categoti_id=categori.id JOIN images ON images.dress_id = dress.id', (err, rows) => {
    if (err) throw err;
    data = rows;
    //console.log('let data = rows;', data);
})


router.get('/:id', (req, res, next) => {
    let id = req.params.id;

    Object.keys(data).forEach(key => {

        if (data[key].id == id) {

            let cart = new Cart(req.session.cart ? req.session.cart : {});
            cart.add(data[key], id);
            req.session.cart = cart;

            res.redirect('/termin');

        }
    });
})

router.get('/reduce/:id', (req, res, next) => {

    let id = req.params.id;
    Object.keys(data).forEach(key => {

        if (data[key].id == id) {
            let cart = new Cart(req.session.cart ? req.session.cart : {});
            cart.delet(id);
            req.session.cart = cart;

            res.redirect('/termin');

        }
    });
})

router.get('/:params', (req, res, next) => {
    let params = req.params.params;
    console.log(params)
    res.render('home/termin/termin', { params: params })
})

router.get('/', (req, res, next) => {
    if (!req.session.cart) {
        return res.render('home/termin/termin', { cart: null })
    }
    let cart = new Cart(req.session.cart);
    console.log('cart = ', cart.items);
    res.render('home/termin/termin', { title: '- termin', cart: cart.items });
})

router.post('/chekout', (req, res) => {
    let clientForm = {
        Name: req.body.clientname,
        email: req.body.email,
        phone: req.body.phoneForm
    };
    let getClientDB = `SELECT * FROM clients WHERE email = '${clientForm.email}'`;
    let query = db.query(getClientDB, (err, getClientRows) => {
        if (err) throw err;
        if (getClientRows.length) {
            console.log('getClientRows', getClientRows[0].id)


            //        console.log('clientId', clientId)
            let TerminForm = {
                client_id: getClientRows[0].id,
                data: req.body.dataForm,
                tarif: req.body.select
            }
            console.log('TerminForm' , TerminForm);
            if(TerminForm.tarif == null){
                TerminForm.tarif = 'silber'
            }
            let createTerminDB = 'INSERT INTO termin SET ?';
            let query = db.query(createTerminDB, [TerminForm], (err, terminRows) => {
                if (err) throw err;

                clientForm.termin_id = terminRows.insertId;
                console.log('clientForm.termin_id', clientForm);
                console.log('terminRows.client_id', TerminForm.client_id);
                let updateClientsDB = `UPDATE clients SET ? WHERE id = '${TerminForm.client_id}'`;
                let query = db.query(updateClientsDB, [clientForm], (err, rezult) => {
                    if (err) throw err;
                    console.log('updateClientsDB = ', rezult)

                    let cart = new Cart(req.session.cart);

                    let createTerminOrder = 'INSERT INTO termindetails SET ?';
                    let dressItem = [];
                    dressItem = Object.keys(cart.items);
                    dressItem.forEach(function (item, i, dressItem) {
                        let terminOrdersItems = {
                            termin_id: clientForm.termin_id,
                            dress_id: item
                        }
                        let query = db.query(createTerminOrder, [terminOrdersItems], (err, terminOrderRows) => {
                            if (err) throw err;
                            console.log('terminOrderRows=', terminOrderRows)
                        })
                    })

                })
            })

        } else {
            let insertQuery = 'INSERT INTO clients SET ?';
            let query = db.query(insertQuery, [clientForm], (err, insertRow) => {
                if (err) throw err;

                let TerminForm = {
                    client_id: insertRow.insertId,
                    data: req.body.dataForm,
                    tarif: req.body.tarif
                }
                let createTerminDB = 'INSERT INTO termin SET ?';
                let query = db.query(createTerminDB, [TerminForm], (err, terminRows) => {
                    if (err) throw err;

                    clientForm.termin_id = terminRows.insertId;
                    console.log('clientForm.termin_id', clientForm);
                    console.log('terminRows.client_id', TerminForm.client_id);
                    let updateClientsDB = `UPDATE clients SET ? WHERE id = '${TerminForm.client_id}'`;
                    let query = db.query(updateClientsDB, [clientForm], (err, rezult) => {
                        if (err) throw err;
                        console.log('updateClientsDB = ', rezult)

                        let cart = new Cart(req.session.cart);

                        let createTerminOrder = 'INSERT INTO termindetails SET ?';
                        let dressItem = [];
                        dressItem = Object.keys(cart.items);
                        dressItem.forEach(function (item, i, dressItem) {
                            let terminOrdersItems = {
                                termin_id: clientForm.termin_id,
                                dress_id: item
                            }
                            let query = db.query(createTerminOrder, [terminOrdersItems], (err, terminOrderRows) => {
                                if (err) throw err;
                                console.log('terminOrderRows=', terminOrderRows)
                            })
                        })

                    })
                })

            })


        }

    })
            
   res.redirect('/');
});


module.exports = router;