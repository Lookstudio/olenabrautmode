const express = require('express');
const router = express.Router();
const path = require('path');
const db = require('../../config/DBconnection');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const { userAuthenticated } = require('../../helpers/authentication');
require('../../config/passport');


router.all('/*',  (req, res, next) => {
	if(req.user){
		console.log(req.user.Name,  req.headers.referer)
	}
	else console.log('quest', req.headers.referer);
	
	req.app.locals.layout = 'home';
	
	res.locals.url = req.url;

    next();
});

router.get('/', (req, res) =>{
	
	/*req.session.admin = res.locals.user ;
	if(req.session.admin){
		console.log(`we found ${req.session.cookie}`);
	}*/

	//let sql = "SELECT dress.id, dress.slug, dress.DressName, dress.Description, images.img_title, images.img_right, images.img_left, images.img_top, silhouette.Title, categori.TitleCat FROM dress JOIN silhouette ON dress.silhuette_id = silhouette.id JOIN categori ON dress.categoti_id=categori.id JOIN images ON images.dress_id = dress.id WHERE dress.id IN ('59', '57', '58', '60')";
	let sql = 'SELECT dress.id, dress.slug, dress.DressName, dress.Description, images.img_title, images.img_right, images.img_left, images.img_top, silhouette.Title, categori.TitleCat FROM dress JOIN silhouette ON dress.silhuette_id = silhouette.id JOIN categori ON dress.categoti_id=categori.id JOIN images ON images.dress_id = dress.id';
	let query = db.query(sql, (err, rows) => {
		if(err) throw err;

	res.render('home/index', {data: rows});
	})
})


router.get('/unsere_dienstleistungen', (req, res) =>{
	res.render('home/unsere_dienstleistungen', {title: '- Unsere Dienstleistungen'});
})

router.get('/atelier', (req, res) =>{
	res.render('home/atelier', {title: '- Atelier'});
})

router.get('/unsere', (req, res) =>{
	res.render('home/unsere', {title: '- Unsere Bräute'});
})

router.get('/kontakt', (req, res) =>{
	res.render('home/kontakt', {title: '- Kontakt'});
})


router.get('/signup', notLoggedIn, (req, res) => {
	res.render('home/signup');
})

router.post('/signup', passport.authenticate('local-signup', {
	successRedirect: '/termin',
	failureRedirect: '/signup',
	failureFlash: true
   }));

router.get('/signin', notLoggedIn, (req, res) => {
	res.render('home/signin');
})


router.post('/signin', passport.authenticate('local-login', {
	successRedirect: '/termin',
	failureRedirect: '/signin',
	failureFlash: true
   }),
	function(req, res){
	 if(req.body.remember){
	  req.session.cookie.maxAge = 1000 * 60 * 3;
	 }else{
	  req.session.cookie.expires = false;
	 }
	 res.redirect('/');
	});



router.get('/logout', (req, res) =>{
	req.logOut();
	res.redirect('/');
})

function notLoggedIn(req, res, next){
		if(!req.isAuthenticated()){
			return next();
		}
		

	res.redirect('/');
}

module.exports = router;