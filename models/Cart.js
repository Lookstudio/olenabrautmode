module.exports = function Cart(oldCart){
    this.items = oldCart.items || {};
    this.totalQty = oldCart.totalQty || 0;

    this.add = function(rows, id){
        let storedItem = this.items[id];
        if(!storedItem){
            storedItem = this.items[id] = {item: rows, qty: 0};
        }
        storedItem.qty++;
        this.totalQty++;
    };

    this.delet = function(id){
        this.totalQty -= this.items[id].qty;
            delete this.items[id];
    };

    this.generateArray = function(){
        let arr = [];
        
        for(let id in this.items){
            arr.push(this.items[id]);
        }
        return arr;
    }
}