const express = require('express');
const app = express();
const path = require('path');
const exphbs = require('express-handlebars');
const mysql = require('mysql');
const connection = require('express-myconnection');
const bodyParses = require('body-parser');
const upload = require('express-fileupload');
const session = require('express-session');
const flash = require('connect-flash');
const cookieParser = require('cookie-parser');
const passport = require('passport');


//load routes
const home = require('./routes/home/index');
const admin = require('./routes/admin/index');
const kleider = require('./routes/admin/kleider');
const kategorien = require('./routes/home/kategorien');
const termin = require('./routes/home/termin');
require('./config/passport')(passport);
//const Dress = require('./models/Dress');

app.use(session({
	secret: 'test admin',
	resave: true,
	saveUninitialized: true 
}));
app.use(cookieParser());

app.use(flash());

// PASSPORT

app.use(passport.initialize());
app.use(passport.session());


//Local variables using middleware
app.use((req, res, next) =>{
	res.locals.user = req.user || null;
	res.locals.session = req.session;

	res.locals.success_message = req.flash('succes-message');
	res.locals.danger_message = req.flash('danger-message');
	res.locals.loginMessage = req.flash('loginMessage');
	res.locals.error = req.flash('error');
	next();
})

app.use(bodyParses.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname,'./public')));

// set view engine
const {select} = require('./helpers/handlebars-helpers');

app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({defaultLayout: 'home', helpers: {select: select}}));
app.set('view engine', 'handlebars');

//Upload Middlewar

app.use(upload());

//use Routes
app.use('/', home);
app.use('/kategorien', kategorien)
app.use('/termin', termin);
app.use('/admin', admin);
app.use('/admin/dress', kleider);


app.listen(4500, () => {

	console.log(`listening on port 4500`);

});