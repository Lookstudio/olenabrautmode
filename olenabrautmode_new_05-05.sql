-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 05 2019 г., 22:23
-- Версия сервера: 5.7.20
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `olenabrautmode`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categori`
--

CREATE TABLE `categori` (
  `id` int(11) NOT NULL,
  `TitleCat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categori`
--

INSERT INTO `categori` (`id`, `TitleCat`) VALUES
(1, 'braut'),
(2, 'abend'),
(3, 'kinder');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `termin_id` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id`, `Name`, `phone`, `email`, `termin_id`, `role_id`) VALUES
(60, 'Kirril', '456789123', 'kirus@gmail.com', 6, 2),
(61, 'Client 45', '457812567895', 'client45@mail.ru', NULL, 2),
(65, 'AlexGum', '+380957102318', 'chaosgum12@gmail.com', 1, 1),
(66, 'Client1', '1112223339', 'client1@gmail.com', NULL, 2),
(67, 'Yuko', '123659754', 'yuko@i.ua', 5, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `dress`
--

CREATE TABLE `dress` (
  `id` int(11) NOT NULL,
  `DressName` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `silhuette_id` int(11) NOT NULL,
  `categoti_id` int(11) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `Price` int(10) NOT NULL,
  `Marke` varchar(55) NOT NULL,
  `Stoff` varchar(55) NOT NULL,
  `Oberteil` varchar(55) NOT NULL,
  `Hinten` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dress`
--

INSERT INTO `dress` (`id`, `DressName`, `Description`, `silhuette_id`, `categoti_id`, `slug`, `Price`, `Marke`, `Stoff`, `Oberteil`, `Hinten`) VALUES
(56, 'AlexGum', 'jk', 2, 1, 'alex-gum', 2000, 'Olena Brautmode', 'Seide', 'V-Ausschnitt', 'runder Rückenausschnitt '),
(57, 'Alex Gum', 'g', 2, 1, 'alex-gum', 1870, 'Olena Brautmode', 'Seide', 'V-Ausschnitt', 'runder Rückenausschnitt '),
(58, 'exepley1', 'fg', 2, 1, 'exepley1', 2000, 'Olena Brautmode', 'Seide', 'V-Ausschnitt', 'runder Rückenausschnitt '),
(59, 'Dress number two', 'ffsdfsdfsdf', 1, 1, 'dress-number-two', 900, 'Olena Brautmode', 'Seide', 'V-Ausschnitt', 'runder Rückenausschnitt '),
(60, 'Dress number 5', '56+4+654985465', 3, 1, 'dress-number-5', 1670, 'Olena Brautmode', 'Seide', 'V-Ausschnitt', 'runder Rückenausschnitt '),
(61, 'Dress seven', '213456', 1, 1, 'dress-seven', 1345, 'Olena Brautmode', 'Seide', 'V-Ausschnitt', 'runder Rückenausschnitt '),
(62, 'Super Braut', 'Meerjungfrau spitze unterlegt mit Hautfarben transparent Stoff, schleppe, blush\r\nOberteil: V-Ausschnitt mit silbernen Glasperlen bestickt. Hinten: runder Rückenausschnitt mit.\r\nsilbernen Glasperlen. Träger, Reißverschluss', 1, 1, 'super-braut', 1523, 'Olena Brautmode', 'Seide', 'V-Ausschnitt', 'runder Rückenausschnitt ');

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE `images` (
  `dress_id` int(11) NOT NULL,
  `img_title` varchar(50) DEFAULT NULL,
  `img_right` varchar(50) DEFAULT NULL,
  `img_left` varchar(50) DEFAULT NULL,
  `img_top` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`dress_id`, `img_title`, `img_right`, `img_left`, `img_top`) VALUES
(57, 'AlexGum-title.jpg', 'AlexGum-left.jpg', 'AlexGum-right.jpg', 'AlexGum-top.jpg'),
(58, 'exepley1-title.jpg', 'exepley1-left.jpg', 'exepley1-right.jpg', 'exepley1-top.jpg'),
(59, 'Dress number two-title.jpg', 'Dress number two-left.jpg', 'Dress number two-right.jpg', 'Dress number two-top.jpg'),
(60, 'Dress number 5-title.jpg', 'Dress number 5-left.jpg', 'Dress number 5-right.jpg', 'Dress number 5-top.jpg'),
(61, 'Dress seven-title.jpg', 'Dress seven-left.jpg', 'Dress seven-right.jpg', 'Dress seven-top.jpg'),
(62, 'Super Braut-title.jpg', 'Super Braut-left.jpg', 'Super Braut-right.jpg', 'Super Braut-top.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `role`
--

INSERT INTO `role` (`id`, `status`) VALUES
(1, 'Admin'),
(2, 'Clients');

-- --------------------------------------------------------

--
-- Структура таблицы `silhouette`
--

CREATE TABLE `silhouette` (
  `id` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `silhouette`
--

INSERT INTO `silhouette` (`id`, `Title`) VALUES
(1, 'Meerjungfrau'),
(2, 'A-Linie'),
(3, 'A-Linie / Prinzessin');

-- --------------------------------------------------------

--
-- Структура таблицы `termin`
--

CREATE TABLE `termin` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `data` date NOT NULL,
  `tarif` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `termin`
--

INSERT INTO `termin` (`id`, `client_id`, `data`, `tarif`) VALUES
(1, 65, '2018-12-19', 'platina'),
(2, 67, '2019-02-24', 'platina'),
(3, 67, '2019-02-24', 'gold'),
(4, 67, '2019-02-25', 'silber'),
(5, 67, '2019-02-22', 'silber'),
(6, 60, '2019-02-28', 'silber');

-- --------------------------------------------------------

--
-- Структура таблицы `termindetails`
--

CREATE TABLE `termindetails` (
  `id` int(11) NOT NULL,
  `termin_id` int(11) NOT NULL,
  `dress_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `termindetails`
--

INSERT INTO `termindetails` (`id`, `termin_id`, `dress_id`) VALUES
(1, 1, 58),
(2, 2, 60),
(3, 3, 60),
(4, 4, 58),
(5, 4, 60),
(6, 6, 57);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categori`
--
ALTER TABLE `categori`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `termin_id` (`termin_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Индексы таблицы `dress`
--
ALTER TABLE `dress`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoti_id` (`categoti_id`),
  ADD KEY `silhuette_id` (`silhuette_id`);

--
-- Индексы таблицы `images`
--
ALTER TABLE `images`
  ADD KEY `images_ibfk_1` (`dress_id`);

--
-- Индексы таблицы `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `silhouette`
--
ALTER TABLE `silhouette`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `termin`
--
ALTER TABLE `termin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`);

--
-- Индексы таблицы `termindetails`
--
ALTER TABLE `termindetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dress_id` (`dress_id`),
  ADD KEY `termin_id` (`termin_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categori`
--
ALTER TABLE `categori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT для таблицы `dress`
--
ALTER TABLE `dress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT для таблицы `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `silhouette`
--
ALTER TABLE `silhouette`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `termin`
--
ALTER TABLE `termin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `termindetails`
--
ALTER TABLE `termindetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`termin_id`) REFERENCES `termin` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `clients_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Ограничения внешнего ключа таблицы `dress`
--
ALTER TABLE `dress`
  ADD CONSTRAINT `dress_ibfk_1` FOREIGN KEY (`categoti_id`) REFERENCES `categori` (`id`),
  ADD CONSTRAINT `dress_ibfk_2` FOREIGN KEY (`silhuette_id`) REFERENCES `silhouette` (`id`);

--
-- Ограничения внешнего ключа таблицы `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_ibfk_1` FOREIGN KEY (`dress_id`) REFERENCES `dress` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `termin`
--
ALTER TABLE `termin`
  ADD CONSTRAINT `termin_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `termindetails`
--
ALTER TABLE `termindetails`
  ADD CONSTRAINT `termindetails_ibfk_1` FOREIGN KEY (`dress_id`) REFERENCES `dress` (`id`),
  ADD CONSTRAINT `termindetails_ibfk_2` FOREIGN KEY (`termin_id`) REFERENCES `termin` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
