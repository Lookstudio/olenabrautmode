
(function ($) {
    "use strict";
    $('.btn-show-menu-mobile').on('click', function(){
        $(this).toggleClass('is-active');
        $('.menu-mobile').slideToggle();
    });

    var arrowMainMenu = $('.arrow-main-menu-m');

    for(var i=0; i<arrowMainMenu.length; i++){
        $(arrowMainMenu[i]).on('click', function(){
            $(this).parent().find('.sub-menu-m').slideToggle();
            $(this).toggleClass('turn-arrow-main-menu-m');
        })
    }

    $(window).resize(function(){
        if($(window).width() >= 992){
            if($('.menu-mobile').css('display') == 'block') {
                $('.menu-mobile').css('display','none');
                $('.btn-show-menu-mobile').toggleClass('is-active');
            }

            $('.sub-menu-m').each(function(){
                if($(this).css('display') == 'block') { console.log('hello');
                    $(this).css('display','none');
                    $(arrowMainMenu).removeClass('turn-arrow-main-menu-m');
                }
            });
                
        }
    });



    /*==================================================================*/

        /*[ Back to top ]
    ===========================================================*/
    var windowH = $(window).height()/2;

    $(window).on('scroll',function(){
        if ($(this).scrollTop() > windowH) {
            $("#myBtn").css('display','flex');
        } else {
            $("#myBtn").css('display','none');
        }
    });

    $('#myBtn').on("click", function(){
        $('html, body').animate({scrollTop: 0}, 300);
    });



    $('.buttonHurt').hover(
           function(){ $(this).addClass('beat')
})


$('.login100-form input').on('change', function() {
    let inputVar = $("input[type='radio']:checked").val();
    let divFront = document.getElementsByClassName(".front");
    if(inputVar == 'silber'){
        $(".front-1").css('opacity', '0');
    }
    else $(".front-1").css('opacity', '1');

    if(inputVar == 'gold'){
        $(".front-2").css('opacity', '0');
    }
    else $(".front-2").css('opacity', '1');

    if(inputVar == 'platina'){
        $(".front-3").css('opacity', '0');
    }
    else $(".front-3").css('opacity', '1');
 });


})(jQuery);

	         /*==================================================================
        [ Slick2 ]*/
		$(document).ready(function() { 
            $('.slick-wrap').slick({
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  autoplay: true,
                  autoplaySpeed: 3000,
                  arrows: true,
                  prevArrow:'<button class="arrow-slick2 prev-slick2"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
                  nextArrow:'<button class="arrow-slick2 next-slick2"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',  
                  responsive: [
                    {
                      breakpoint: 1200,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 3
                      }
                    },
                    {
                      breakpoint: 992,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 768,
                      settings: {
                        arrows: false,
                        slidesToShow: 1,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 576,
                      settings: {
                        arrows: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]    
                });
              
            });
    
      /*==================================================================
    [ Isotope ]*/
    
    $(window).on('load', function () {
    var $topeContainer = $('.isotope-grid');
    var $filter = $('.filter-tope-group');

    // filter items on button click
    $filter.each(function () {
        $filter.on('click', 'button', function () {
            var filterValue = $(this).attr('data-filter');
            $topeContainer.isotope({filter: filterValue});
        });
        
    });

    // init Isotope
        var $grid = $topeContainer.each(function () {
            $(this).isotope({
                itemSelector: '.isotope-item',
                layoutMode: 'fitRows',
                percentPosition: true,
                animationEngine : 'best-available',
                masonry: {
                    columnWidth: '.isotope-item'
                }
            });
        });
  

    var isotopeButton = $('.filter-tope-group button');

    $(isotopeButton).each(function(){
        $(this).on('click', function(){
            for(var i=0; i<isotopeButton.length; i++) {
                $(isotopeButton[i]).removeClass('filter-button-active');
            }

            $(this).addClass('filter-button-active');
        });
    });
});